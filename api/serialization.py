import contextlib
from itertools import starmap
from django.apps import apps
from django.core.exceptions import ValidationError

from api.representation import OpenFoodApi


class OpenSerializer(OpenFoodApi):
    def __init__(self, api_res):
        self.app_name = 'substitute'
        (
            self.ProductModel,
            self.CategoryModel,
            self.StoreModel,
            self.BrandModel
        ) = tuple(
            starmap(
                apps.get_model,
                (
                    (self.app_name, 'Product'),
                    (self.app_name, 'Category'),
                    (self.app_name, 'Store'),
                    (self.app_name, 'Brand')
                )
            )
        )
        self.data = self.get_openfood_data(api_res)

    def get_openfood_data(self, api_res):
        p_unsaved, cats, stores, brands = [], [], [], []
        for prods in api_res:
            for prod in prods:
                self.build_unsaved_prods(p_unsaved, prod)
                cats += prod['categories']
                stores += prod['stores_tags']
                brands += prod['brands_tags']
        c_unsaved, s_unsaved, b_unsaved = tuple(
            starmap(
                self.build_unsaved_inst,
                (
                    (cats, self.CategoryModel),
                    (stores, self.StoreModel),
                    (brands, self.BrandModel)
                )
            )
        )
        return api_res, p_unsaved, c_unsaved, s_unsaved, b_unsaved

    def build_unsaved_prods(self, p_unsaved, prod):
        with contextlib.suppress(KeyError):
            # catch with unvalid
            prod_unsaved_inst = self.ProductModel(
                            name=prod['generic_name_fr'],
                            url=prod['url'],
                            nutriscore=prod['nutriscore_grade'],
                            barcode=prod['code'],
                            image_url=prod['image_front_url']
            )
            if not self.catch_unvalid(prod_unsaved_inst):
                p_unsaved.append(
                    (
                        prod_unsaved_inst
                    )
                )

    def build_unsaved_inst(self, related_data, a_model):
        return [
            inst for inst in [
                a_model(name=inst_name)
                for inst_name in set(related_data)
            ]
            if not self.catch_unvalid(inst)
        ]

    @staticmethod
    def catch_unvalid(inst):
        try:
            inst.full_clean()
            return False
        except ValidationError:
            return True
