from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path

from substitute.views import HomeView


urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('users.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('', HomeView.as_view(), name='home'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
