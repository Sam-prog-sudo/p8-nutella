import factory

from users.models import UserProfile


class UserProfileFactory(factory.django.DjangoModelFactory):
    """
    Dummy factory to create an object of ParentGroup model.
    """
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    email = username = factory.LazyAttribute(
        lambda user: '%s@example.com' % user.first_name.lower()
    )
    password = "secret"
    is_staff = False
    is_superuser = False

    class Meta:
        model = UserProfile
