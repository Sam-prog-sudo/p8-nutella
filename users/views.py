from django.urls import reverse_lazy
from django.views import generic

from users.forms import EmailUserCreationFrom


class SignUpView(generic.CreateView):
    form_class = EmailUserCreationFrom
    success_url = reverse_lazy('login')
    template_name = 'registration/signup.html'
