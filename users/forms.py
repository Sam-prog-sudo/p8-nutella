from django import forms
from django.contrib.auth.forms import UserCreationForm

from users.models import UserProfile


class EmailUserCreationFrom(UserCreationForm):
    username = forms.EmailField(help_text="Enter a valid email address.")

    class Meta:
        model = UserProfile
        fields = ('username', 'password1', 'password2')
