# Nutella


Suivez [ce lien]("#") pour utiliser l'application et profitez-en !

[![image](https://img.shields.io/github/license/Sam-prog-sudo/sam.github.io?style=flat-square)](https://gitlab.com/oc-sam/papy-bot/-/blob/main/LICENSE)

## Table des matières  

- [Contraintes](#contraintes)  
- [Cahier des charges](#cahier-des-charges)  
  - [Fonctionnalités](#fonctionnalités)  
  - [Parcours utilisateur](#parcours-utilisateur)  
- [Améliorations possibles](#améliorations-possibles)  
- [License](#license)  

---

## Contraintes


---

## Cahier des charges

### Fonctionnalités



### Parcours utilisateur


---

## Améliorations possibles



## License

**[MIT license]("#")**
