from django.contrib import admin
from substitute.models import Product, Store, Category, Favorite


admin.site.register(Product)
admin.site.register(Category)
admin.site.register(Store)
admin.site.register(Favorite)
