import contextlib

from substitute.models import Brand, Category, Product, Store


class BulkCreator:
    def __init__(self) -> None:
        self.all_models = Product, Category, Store, Brand
        self.inter_models = {
            'cats': Product.categories.through,
            'stores': Product.stores.through,
            'brands': Product.brands.through,
        }

    def primary(self, unsaved_instances):
        model_inst_coresp = zip(self.all_models, unsaved_instances)
        saved_inst = []
        for a_model, unsaved_inst in model_inst_coresp:
            a_model.objects.bulk_create(unsaved_inst, ignore_conflicts=True)
            saved_inst.append(
                list(a_model.objects.filter(
                    name__in=[inst.name for inst in unsaved_inst]
                ))
            )
        return saved_inst

    def get_instances(
        self, all_prods, p_inst_saved, c_inst_saved, s_inst_saved, b_inst_saved
    ):
        inst_dict = {}
        for prod_inst in p_inst_saved:
            for prods in all_prods:
                for prod in prods:
                    with contextlib.suppress(KeyError):
                        if prod_inst.name == prod['generic_name_fr']:
                            inst_dict[prod_inst.name] = {
                                'cats': [],
                                'stores': [],
                                'brands': []
                            }
                        for saved_instances, data_key, dict_key in [
                            (c_inst_saved, 'categories', 'cats'),
                            (s_inst_saved, 'stores_tags', 'stores'),
                            (b_inst_saved, 'brands_tags', 'brands')
                        ]:
                            self._append_instances(
                                prod,
                                saved_instances,
                                data_key,
                                dict_key,
                                inst_dict,
                                prod_inst
                            )
        return inst_dict

    @staticmethod
    def _append_instances(
        prod_data, saved_instances, data_key, dict_key, inst_dict, prod_inst
    ):
        for inst in saved_instances:
            if inst.name in prod_data[data_key]:
                inst_dict[prod_inst.name][dict_key].append(inst)
        inst_dict[prod_inst.name]['prod'] = prod_inst

    def list_unsaved_instances(self, related_name, related):
        if related_name == 'cats':
            return [
                self.inter_models[related_name](
                    product=related['prod'],
                    category=related_cat
                )
                for related_cat in related[related_name]
            ]
        if related_name == 'stores':
            return [
                self.inter_models[related_name](
                    product=related['prod'],
                    store=related_store
                )
                for related_store in related[related_name]
            ]
        if related_name == 'brands':
            return [
                self.inter_models[related_name](
                    product=related['prod'],
                    brand=related_brand
                )
                for related_brand in related[related_name]
            ]

    def intermediaries(self, inst_dict):
        for related in inst_dict.values():
            for related_name in related.keys():
                if related_name != 'prod':
                    self.inter_models[related_name].objects.bulk_create(
                        self.list_unsaved_instances(related_name, related),
                        ignore_conflicts=True
                    )
