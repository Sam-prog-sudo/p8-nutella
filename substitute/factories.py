import factory

from substitute.models import Category, Favorite, Product, Store
from users.factory import UserProfileFactory


class ProductFactory(factory.django.DjangoModelFactory):

    name = factory.Faker('word')
    url = factory.Faker("url")
    nutriscore = factory.Iterator(['a', 'b', 'c', 'd', 'e', 'f'])

    class Meta:
        model = Product


class StoreFactory(factory.django.DjangoModelFactory):
    name = factory.Faker('company')
    address = factory.Faker('address')

    @factory.post_generation
    def products(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for product in extracted:
                self.products.add(product)

    class Meta:
        model = Store


class CategoryFactory(factory.django.DjangoModelFactory):
    name = factory.Faker('word')

    @factory.post_generation
    def products(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for product in extracted:
                self.products.add(product)

    class Meta:
        model = Category


class FavoriteFactory(factory.django.DjangoModelFactory):
    user = factory.SubFactory(UserProfileFactory)
    product = factory.SubFactory(ProductFactory)
    subsitute = factory.SubFactory(ProductFactory)

    class Meta:
        model = Favorite
