from django.db import models


class Product(models.Model):
    name = models.CharField(max_length=255)
    url = models.URLField()
    nutriscore = models.CharField(max_length=1)
    barcode = models.CharField(max_length=255, unique=True)
    image_url = models.URLField()

    def __str__(self):
        return f"{self.name}"


class Favorite(models.Model):
    user = models.ForeignKey(
        "users.UserProfile",
        related_name=("favorites"),
        on_delete=models.CASCADE
    )
    product = models.ForeignKey(
        "Product",
        related_name=("favorites"),
        on_delete=models.CASCADE
    )
    subsitute = models.ForeignKey(
        "Product",
        related_name=("is_substitute_of"),
        on_delete=models.CASCADE
    )


class Store(models.Model):
    name = models.CharField(max_length=255, unique=True)
    products = models.ManyToManyField(
        "Product", related_name="stores", blank=True
    )

    def __str__(self):
        return f"{self.name}"


class Category(models.Model):
    name = models.CharField(max_length=255, unique=True)
    products = models.ManyToManyField(
        "Product", related_name="categories", blank=True
    )

    def __str__(self):
        return f"{self.name}"

    class Meta:
        verbose_name_plural = "categories"


class Brand(models.Model):
    name = models.CharField(max_length=255, unique=True)
    products = models.ManyToManyField(
        "Product", related_name="brands", blank=True
    )

    def __str__(self):
        return f"{self.name}"
